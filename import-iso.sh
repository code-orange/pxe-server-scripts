#!/bin/bash

set -e
set -x

TEMP_NAME=`echo $RANDOM | md5sum | head -c 20`

cd "$(dirname "$0")"

mkdir -p template/iso-import/${TEMP_NAME}
cd template/iso-import/${TEMP_NAME}

axel -n 20 "$1"

mv ./* /data/template/iso/

cd "$(dirname "$0")"
rm -rf template/iso-import/${TEMP_NAME}

exit 0

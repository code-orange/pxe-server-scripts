#!/bin/bash

set -e
set -x

NUM_JOBS=$(expr $(nproc) + $(nproc))

TARGETS="bin-i386-pcbios/ipxe.dsk bin-i386-efi/ipxe.efi bin-i386-pcbios/ipxe.iso bin-i386-pcbios/ipxe.lkrn bin-i386-pcbios/ipxe.pxe bin-i386-pcbios/ipxe.usb bin-i386-pcbios/undionly.kpxe"

git clone https://github.com/ipxe/ipxe.git
cd ipxe/src

sed 's,//#define CONSOLE_CMD,#define CONSOLE_CMD,g' -i config/general.h
sed 's,//#define	CONSOLE_FRAMEBUFFER,#define	CONSOLE_FRAMEBUFFER,g' -i config/console.h

make -j$NUM_JOBS

for target in $TARGETS
do
    make -j$NUM_JOBS $target
    cp $target ../../
done

cd ../../

rm -rf ipxe/

git add -A

git commit -m "update iPXE binaries"

exit 0

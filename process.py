from os import listdir, remove
from os.path import exists, isfile, join
from pathlib import Path

import requests

IMAGE_SERVER_FQDN = "install.srvfarm.net"
IMAGE_SERVER_USER = "install"
IMAGE_SERVER_PASSWD = "install"

session = requests.Session()
session.auth = (IMAGE_SERVER_USER, IMAGE_SERVER_PASSWD)

image_list_url = "https://{}/json/template/iso/".format(IMAGE_SERVER_FQDN)

images_request = session.get(
    image_list_url,
)

if images_request.status_code != 200:
    exit(1)

images = images_request.json()

fstab = open("template/mount/fstab", "wt")
ipxe_menu = open("boot.ipxe", "wt")

ipxe_menu_content = str()
ipxe_item_content = str()

ipxe_menu.write("#!ipxe" + "\n\n")

ipxe_menu.write("console --x 1024 --y 768 ||" + "\n")
ipxe_menu.write(
    "console --picture http://install:install@install.srvfarm.net/data/ipxe.png --left 32 --right 32 --top 32 --bottom 48 ||"
    + "\n\n"
)

ipxe_menu_content += ":menu" + "\n"
ipxe_menu_content += "menu Code Orange Boot Menu" + "\n"

ipxe_menu_content += "item --key n normal BOOT FROM HARDDISK" + "\n"
ipxe_menu_content += "item --key s shell iPXE SHELL" + "\n"
ipxe_menu_content += "item --key r reboot REBOOT" + "\n"

ipxe_menu_content += "item --gap -- --------------------------------------" + "\n"

image_names_without_iso = list()

for image in images:
    print(image["name"])

    image_name = image["name"]
    image_name_without_iso = ".".join(image["name"].split(".")[:-1])

    image_names_without_iso.append(image_name_without_iso)

    # IMAGE FSTAB
    fstab.write(
        "/data/template/iso/{} /data/template/mount/{} udf,iso9660 loop,ro,x-mount.mkdir 0 0".format(
            image_name, image_name_without_iso
        )
        + "\n"
    )

    # IPXE NAV
    ipxe_template_name_file_path = "template/ipxe/{}".format(image_name_without_iso)

    if not exists(ipxe_template_name_file_path):
        ipxe_template_name_file = open(ipxe_template_name_file_path, "wt")
        ipxe_template_name_file.write("OTHER")
        ipxe_template_name_file.close()

    ipxe_template_name = open(ipxe_template_name_file_path, "rt").read().strip()

    # BOOT TEMPLATE NAME
    config_template_name_file_path = "template/config/{}".format(image_name_without_iso)

    if not exists(config_template_name_file_path):
        config_template_name_file = open(config_template_name_file_path, "wt")
        config_template_name_file.write("sanboot")
        config_template_name_file.close()

    config_template_name = open(config_template_name_file_path, "rt").read().strip()

    # Generate iPXE menu
    ipxe_item_content += ":{}".format(image_name_without_iso) + "\n"

    snippet = open(
        "template/ipxe_templates/{}/snippet.ipxe".format(
            config_template_name,
        ),
        "rt",
    ).read()

    snippet = snippet.replace("$$FILE_SERVER_FQDN$$", IMAGE_SERVER_FQDN)
    snippet = snippet.replace("$$IMAGE_NAME_WITHOUT_ISO$$", image_name_without_iso)
    snippet = snippet.replace("$$IMAGE_NAME$$", image_name)

    ipxe_item_content += snippet

    ipxe_item_content += "goto menu" + "\n\n"

    ipxe_menu_content += (
        "item {} {}".format(
            image_name_without_iso,
            image_name_without_iso.replace("_", " ").upper(),
        )
        + "\n"
    )

    # template specific code
    if config_template_name == "windows":
        init_scripts_path = "template/init_scripts/{}".format(image_name_without_iso)
        Path(init_scripts_path).mkdir(parents=True, exist_ok=True)

        startup_bat_content = open(
            "template/ipxe_templates/windows/startup.bat", "rt"
        ).read()
        startup_bat_content = startup_bat_content.replace(
            "$$FILE_SERVER_FQDN$$", IMAGE_SERVER_FQDN
        )
        startup_bat_content = startup_bat_content.replace(
            "$$IMAGE_NAME_WITHOUT_ISO$$", image_name_without_iso
        )
        startup_bat_content = startup_bat_content.replace("$$IMAGE_NAME$$", image_name)

        with open(init_scripts_path + "/startup.bat", "wt") as startup_bat:
            startup_bat.write(startup_bat_content)

        winpeshl_ini_content = open(
            "template/ipxe_templates/windows/winpeshl.ini", "rt"
        ).read()
        winpeshl_ini_content = winpeshl_ini_content.replace(
            "$$FILE_SERVER_FQDN$$", IMAGE_SERVER_FQDN
        )
        winpeshl_ini_content = winpeshl_ini_content.replace(
            "$$IMAGE_NAME_WITHOUT_ISO$$", image_name_without_iso
        )
        winpeshl_ini_content = winpeshl_ini_content.replace(
            "$$IMAGE_NAME$$", image_name
        )

        with open(init_scripts_path + "/winpeshl.ini", "wt") as winpeshl_ini:
            winpeshl_ini.write(winpeshl_ini_content)

ipxe_menu_content += (
    "choose --default normal --timeout 10000 target && goto ${target} || goto normal"
    + "\n\n"
)

ipxe_menu.write(ipxe_menu_content)
ipxe_menu.write(ipxe_item_content)

ipxe_menu.write(":normal" + "\n")
ipxe_menu.write("sanboot --no-describe --drive 0x80 || goto fail" + "\n")
ipxe_menu.write("goto menu" + "\n\n")

ipxe_menu.write(":fail" + "\n")
ipxe_menu.write("prompt ERROR Press key to continue" + "\n")
ipxe_menu.write("goto menu" + "\n\n")

ipxe_menu.write(":shell" + "\n")
ipxe_menu.write("shell" + "\n\n")

ipxe_menu.write(":reboot" + "\n")
ipxe_menu.write("reboot" + "\n\n")

# cleanup old config files
ipxe_templates = [
    f for f in listdir("template/ipxe/") if isfile(join("template/ipxe/", f))
]
config_templates = [
    f for f in listdir("template/config/") if isfile(join("template/config/", f))
]

for ipxe_template in ipxe_templates:
    if ipxe_template not in image_names_without_iso:
        remove("template/ipxe/{}".format(ipxe_template))

for config_template in config_templates:
    if config_template not in image_names_without_iso:
        remove("template/config/{}".format(config_template))

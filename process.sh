#!/bin/bash

set -e
set -x

cd "$(dirname "$0")"

echo "CHECK INSTALL"
/usr/local/bin/pip3 install -r requirements.txt

echo "CLEANUP"
chown -R install /data || true
chmod -R +r /data || true

find template/iso/ -depth -exec bash -c '
  for old_name; do
    new_name=$(tr "[:upper:]" "[:lower:]" <<<"$old_name")
    [[ $old_name = "$new_name" ]] && continue
    mv -f "$old_name" "$new_name" || true
  done
' _ {} +;

find template/iso/ -depth -exec bash -c '
  for old_name; do
    new_name=$(tr "-" "_" <<<"$old_name")
    [[ $old_name = "$new_name" ]] && continue
    mv -f "$old_name" "$new_name" || true
  done
' _ {} +;

echo "INDEX LIBRARY"
/usr/bin/python3 process.py

echo "MOUNT IMAGES"
/usr/bin/mount -a -T /etc/fstab.d/images.fstab

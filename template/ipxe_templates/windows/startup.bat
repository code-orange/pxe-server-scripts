@echo on

echo INIT ENVIRONMENT
wpeinit

echo INIT NETWORK - WILL TAKE SOME TIME
wpeutil initializenetwork
ping 127.0.0.1 /n 30 > NULL

echo CONNECT INSTALL SHARE
net use S: \\$$FILE_SERVER_FQDN$$\data install /user:install.srvfarm.net\install
S:\template\mount\$$IMAGE_NAME_WITHOUT_ISO$$\setup.exe
call cmd.exe
